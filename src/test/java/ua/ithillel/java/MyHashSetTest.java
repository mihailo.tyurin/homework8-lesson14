package ua.ithillel.java;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyHashSetTest {

    private static MyHashSet<String> LIST1;
    private static MyHashSet<String> LIST2;
    private static MyHashSet<String> LIST3;

    @BeforeEach
    void prepareData() {
        LIST1 = new MyHashSet<>();
        LIST2 = new MyHashSet<>();
        LIST3 = new MyHashSet<>();

        LIST1.add("zero");
        LIST1.add("one");
        LIST1.add("two");
        LIST1.add("three");
        LIST1.add("four");
        LIST1.add("five");

        LIST2.add("ten");
        LIST2.add("eleven");
        LIST2.add("twelve");

        LIST3.add("zero");
        LIST3.add("two");
        LIST3.add("five");

    }

    @Test
    void shouldGetSize() {
        assertEquals(6, LIST1.size());
        LIST1.add("hello");
        assertEquals(7, LIST1.size());
    }

    @Test
    void shouldCheckIsEmpty() {
        assertFalse(LIST1.isEmpty());
        assertEquals(6, LIST1.size());
        LIST1.clear();
        assertTrue(LIST1.isEmpty());
        assertEquals(0, LIST1.size());
    }

    @Test
    void shouldCheckElementOnContains() {
        boolean isContain = LIST1.contains("five");
        assertTrue(isContain);
        isContain = LIST1.contains("hello");
        assertFalse(isContain);
    }

    @Test
    void toArray() {
        Object[] strings = LIST1.toArray();
        assertEquals(6, strings.length);
    }

    @Test
    void shouldAddElement() {
        assertEquals(6, LIST1.size());
        assertFalse(LIST1.contains("hello"));
        assertFalse(LIST1.contains("world"));

        LIST1.add("hello");
        LIST1.add("world");
        assertEquals(8, LIST1.size());
        assertTrue(LIST1.contains("hello"));
        assertTrue(LIST1.contains("world"));
        assertFalse(LIST1.add("hello"));
    }

    @Test
    void shouldRemoveElement() {
        assertEquals(6, LIST1.size());
        assertTrue(LIST1.contains("one"));

        boolean isRemoved = LIST1.remove("one");
        assertTrue(isRemoved);
        assertEquals(5, LIST1.size());
        assertFalse(LIST1.contains("one"));
        assertFalse(LIST1.contains("hello"));
        isRemoved = LIST1.remove("hello");
        assertFalse(isRemoved);
    }

    @Test
    void shouldCheckAllElementOnContains() {
        assertFalse(LIST1.containsAll(LIST2));
        assertTrue(LIST1.containsAll(LIST3));
    }

    @Test
    void shouldAddAllElement() {
        assertEquals(6, LIST1.size());
        assertFalse(LIST1.contains("ten"));
        assertTrue(LIST1.addAll(LIST2));
        assertEquals(9, LIST1.size());
        assertTrue(LIST1.contains("ten"));
        assertFalse(LIST1.addAll(LIST3));
    }

    @Test
    void testRetainAll() {
        assertFalse(LIST1.retainAll(LIST2));
        LIST2.addAll(LIST3);
        assertTrue(LIST1.retainAll(LIST2));
        assertEquals(3, LIST1.size());
    }

    @Test
    void shouldRemoveAllElement() {
        assertEquals(6, LIST1.size());
        assertTrue(LIST1.contains("zero"));
        LIST1.removeAll(LIST3);
        assertEquals(3, LIST1.size());
        assertFalse(LIST1.contains("zero"));
    }

    @Test
    void shouldClear() {
        assertEquals(6, LIST1.size());
        assertTrue(LIST1.contains("zero"));
        LIST1.clear();
        assertEquals(0, LIST1.size());
        assertFalse(LIST1.contains("zero"));
    }

    @Test
    void iterator() {
    }

    @Test
    void testToArray() {
    }
}