package ua.ithillel.java;


public class Main {
    public static void main(String[] args) {

        MyHashSet<String> myHashSet = new MyHashSet<>();

        myHashSet.add("zero");
        myHashSet.add("one");
        myHashSet.add("two");
        myHashSet.add("three");
        myHashSet.add("four");
        myHashSet.add("five");

        MyHashSet<String> myHashSet1 = new MyHashSet<>();

        myHashSet1.add("zero");
        myHashSet1.add("two");
        myHashSet1.add("three");
        myHashSet1.add("five");
        myHashSet1.add("qwe");
        myHashSet1.add("qwe2");
        myHashSet1.add("qwe3");
        myHashSet1.add("qwe4");
        myHashSet1.add("qwe5");
        myHashSet1.add("qwe6");

        System.out.println();
        System.out.println("***************");
        System.out.println();

        System.out.println(myHashSet.retainAll(myHashSet1));
        System.out.println(myHashSet.size());
        System.out.println();

        for (String s : myHashSet) {
            System.out.println(s);
        }
    }
}
