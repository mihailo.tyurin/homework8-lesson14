package ua.ithillel.java;

import java.util.*;

public class MyHashSet<T> implements Set<T> {
    private static final int DEFAULT_CAPACITY = 16;

    private static class Node {
        Object value;
        Node next;

        public Node(Object value) {
            this.value = value;
        }

        boolean hasValue(Object value) {
            return this.value.equals(value);
        }
    }

    private Node[] nodes;
    private int size = 0;
    private int capacity;

    public MyHashSet() {
        this(DEFAULT_CAPACITY);
    }

    public MyHashSet(int capacity) {
        this.capacity = capacity;
        this.nodes = new Node[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (o == null) return false;
        int index = getIndex(o);
        for (Node cur = nodes[index]; cur != null; cur = cur.next) {
            if (cur.hasValue(o)) {
                return true;
            }
        }
        return false;
    }

    private int getIndex(Object o) {
        return Math.abs(o.hashCode() % nodes.length);
    }

    @Override
    public Iterator<T> iterator() {
        return new MyHashSetIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[size];
        int i = 0;
        Iterator<T> it = iterator();
        while (it.hasNext()) {
            arr[i++] = it.next();
        }
        return arr;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] arrCopy = Arrays.copyOf(a, size);
        int i = 0;
        Iterator<T1> it = (Iterator<T1>) iterator();
        while (it.hasNext()) {
            arrCopy[i++] = it.next();
        }
        return arrCopy;
    }

    @Override
    public boolean add(T value) {
        if (value == null) return false;

        Node node = new Node(value);
        int index = getIndex(value);

        if (nodes[index] == null) {
            nodes[index] = node;
            size++;
            return true;
        }

        for (Node cur = nodes[index]; cur != null; cur = cur.next) {
            if (cur.hasValue(value)) {
                cur.value = value;
                return false;
            } else if (cur.next == null) {
                cur.next = node;
                size++;
                break;
            }
        }

        return true;
    }

    @Override
    public boolean remove(Object o) {
        int index = getIndex(o);
        if (nodes[index] == null) {
            return false;
        }
        Node prev = null;
        for (Node cur = nodes[index]; cur != null; prev = cur, cur = cur.next) {
            if (cur.hasValue(o)) {
                size--;
                if (cur == nodes[index]) {
                    nodes[index] = cur.next;
                } else {
                    prev.next = cur.next;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        int countTrue = 0;
        for (T t : c) {
            if (add(t)) {
                countTrue++;
            }
        }
        return countTrue > 0;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Node[] tmp = new Node[nodes.length];
        int sizeNew = 0;
        int countTrue = 0;
        for (Object value : c) {
            if (contains(value)) {
                Node node = new Node(value);
                int index = getIndex(value);
                if (tmp[index] == null) {
                    tmp[index] = node;
                    sizeNew++;
                    countTrue++;
                }
                for (Node cur = tmp[index]; cur != null; cur = cur.next) {
                    if (cur.hasValue(value)) {
                        cur.value = value;
                    } else if (cur.next == null) {
                        cur.next = node;
                        sizeNew++;
                        countTrue++;
                        break;
                    }
                }
            }
        }
        if (countTrue > 0) {
            size = sizeNew;
            nodes = tmp;
            return true;
        } else return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        int countTrue = 0;
        for (Object o : c) {
            if (remove(o)) {
                countTrue++;
            }
        }
        return countTrue > 0;
    }

    @Override
    public void clear() {
        this.nodes = new Node[capacity];
        this.size = 0;
    }

    private class MyHashSetIterator implements Iterator<T> {
        int index = -1;
        int count = 0;
        Node cur = null;


        @Override
        public boolean hasNext() {
            return count < size;
        }

        private void findNext() {
            if (cur != null && cur.next != null) {
                cur = cur.next;
            } else {
                do {
                    cur = nodes[++index];
                } while (cur == null && index < nodes.length - 1);
            }
        }

        @Override
        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            findNext();
            count++;
            return (T) cur.value;
        }
    }
}

